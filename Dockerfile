FROM adoptopenjdk:11-openj9

WORKDIR /usr/src/app

COPY . .

RUN sh mvnw package

CMD ["java","-jar","target/spring-java-template-0.1.0.jar"]