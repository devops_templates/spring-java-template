#!/usr/bin/bash
./mvnw package && java -jar target/gs-spring-boot-docker-0.1.0.jar
docker build -t spring-java-template .
docker run -it -p 1234:8080 spring-java-template
